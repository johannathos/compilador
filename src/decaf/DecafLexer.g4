lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';
LPAR: '(';
RPAR: ')';
LCOL: '[';
RCOL: ']';
VIRG: ',';
PONTVIRG: ';';
BOOLEAN : 'boolean';
EQ: '=';
PEQ: '+=';
MEQ: '-=';
MEN: '-';
EXCL: '!';
BREAK : 'break';
ARITH: '+' | '-' | '*' | '/' | '%';
REL: '<' | '>' | '<=' | '>=';
EQOP: '==' | '!=';
COND: '&&' | '||';

CALLOUT : 'callout';

CLASS : 'class';

CONTINUE : 'continue';

ELSE : 'else';

FALSE : 'false';

FOR : 'for';

INT : 'int';

IF : 'if';

RETURN : 'return';

TRUE : 'true';

VOID : 'void';

fragment ZERO : '0' INTLITERAL | '0' NUMBER*;

NUM : ZERO | NOVE NUMBER*;

ID  : LETRA ('a'..'z' | 'A'..'Z'|'0'..'9'|'_')*;

WS_ : (' ' | '\n' |'\t' ) -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHARLITERAL : '\'' (ESC|CHAR) '\'';

INTLITERAL : 'x'(NUMBER|ID)+;

STRING : '\"' (ESC|CHAR)* '\"';

OPERADORES : ('|');

fragment ESC :  '\\' ('n'|'"'|'t'|'\\'|'\'');

fragment LETRA : 'a'..'z' |'A'..'Z'|'_';

fragment NUMBER : ('0'..'9');

fragment NOVE : ('1'..'9');

fragment CHAR: ('A'..'Z'|'0'..'9'|'a'..'z'|'_'|'!'|'#'|'$'|'('|')'|'*'|','|'+'|'.'|'/'|'^'|'_'|'`'|'{'|'}'|'|'|'~'|' '|':'|'?'|'%'|'/');

