parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;

}

program: TK_class LCURLY field_decl* method_decl* RCURLY;

field_decl: (type_id | type_id LCOL int_literal RCOL) (VIRG type_id | VIRG type_id LCOL int_literal RCOL)*  PONTVIRG;

method_decl: type ID LPAR  (type_id (VIRG type_id)*)*  RPAR block;

type_id: type? ID; 

block: LCURLY var_decl* statement* RCURLY;

var_decl: type_id (VIRG type_id)* PONTVIRG;

type : INT | BOOLEAN | VOID;

statement:  location assign_op expr PONTVIRG
			| method_call PONTVIRG
			| IF LPAR expr RPAR block (ELSE block)*
			| FOR id EQ expr VIRG expr block
			| RETURN expr* PONTVIRG
			| BREAK PONTVIRG
			| CONTINUE PONTVIRG
			| block;

assign_op: EQ | PEQ | MEQ;


expr : location | method_call
	   | literal | expr bin_op expr
	   | MEN expr | EXCL expr | LPAR expr* RPAR | id expr | id ARITH expr* id*;

method_name: id;

method_call: method_name LPAR (expr(VIRG expr)*)* RPAR
			| CALLOUT LPAR string_literal (VIRG callout_arg)* RPAR;

location: id | id LCOL expr RCOL;

callout_arg: expr | string_literal;

bin_op: arith_op | rel_op | eq_op | cond_op;

arith_op: ARITH;

rel_op: REL;

eq_op: EQOP;

cond_op: COND;

literal: int_literal | char_literal | bool_literal;

id: ID;

hex_literal: NUM; 

int_literal: NUM;

bool_literal: TRUE | FALSE;

char_literal: CHARLITERAL;

string_literal: STRING;



































